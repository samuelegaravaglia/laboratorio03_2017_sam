package it.unimi.di.sweng.lab03;

public class IntegerNode {
	
	
	
	private Integer value = null;
	private IntegerNode next = null;
	
	public IntegerNode(int value) {
		this.value = value;
	}

	public IntegerNode next() {
		return next;
	}

	public void setNext(IntegerNode integerNode) {
		next = integerNode;
	}

	public Integer getValue() {
		return value;
	}
}
