package it.unimi.di.sweng.lab03;

public class IntegerList {
	
	private IntegerNode head = null;
	private IntegerNode tail = null;
	
	//costruttore con string
	public IntegerList(String s) {
		int i; int c=-1;
			for(i=0;i<s.length();i++){
				c=(int)s.charAt(i)-48;
				if(c>=0 && c<=9)
					addLast(c);
				else if (c==-16){
					
				}
				else throw new IllegalArgumentException("Errore");}
	}
	//costruttore senza parametri
	public IntegerList() {
	}
	
	@Override
	public String toString(){
		StringBuilder result = new StringBuilder("[");
		IntegerNode currentNode =head;
		int i=0;
		while (currentNode != null){
			if (i++ >0){
				result.append(' ');
			}
			result.append(currentNode.getValue());
			currentNode = currentNode.next();
		}
		return result.append(']').toString();
	}

	
	
	
	
	public void addLast(int value) {
		if (head == null){
			head = tail = new IntegerNode(value);
		} else {
			IntegerNode node = new IntegerNode(value);
			tail.setNext(node);
			tail = node;
		}
	}
	
	
	
}
